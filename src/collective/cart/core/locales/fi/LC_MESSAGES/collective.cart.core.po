msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2013-11-06 07:33+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI +ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0\n"
"Language-Code: fi\n"
"Language-Name: Finnish\n"
"Preferred-Encodings: utf-8 latin1\n"
"Domain: collective.cart.core\n"

#: ./profiles/default/types/collective.cart.core.Article.xml
msgid "Article"
msgstr "Tuote"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Canceled"
msgstr "Peruttu"

#: ./browser/view.py:24
#: ./portlets/cart.py:20
#: ./profiles/default/portlets.xml
msgid "Cart"
msgstr "Ostoskori"

#: ./profiles/default/types/collective.cart.core.CartArticle.xml
msgid "Cart Article"
msgstr "Ostoskorituote"

#: ./profiles/default/types/collective.cart.core.CartContainer.xml
msgid "Cart Container"
msgstr "Ostoskorikonteineri"

#: ./browser/viewlets/order-listing.pt:15
msgid "Change State"
msgstr "Muuta tila"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Charged"
msgstr "Laskutettu"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Created"
msgstr "Luotu"

#: ./schema.py:23
msgid "Description"
msgstr "Kuvaus"

#: ./portlets/cart.pt:9
msgid "Go to cart"
msgstr "Siirry ostoskoriin"

#: ./browser/viewlets/order-article-listing.pt:11
msgid "ID"
msgstr "Tunnus"

#: ./profiles/default/actions.xml
msgid "Make Shopping Site"
msgstr "Luo kauppa"

#: ./profiles/default/actions.xml
msgid "Make this container shopping site."
msgstr "Tee tästä konteinerista kauppa."

#: ./browser/viewlets/cart-article-listing.pt:12
#: ./browser/viewlets/order-article-listing.pt:12
msgid "Name"
msgstr "Nimi"

#: ./schema.py:14
msgid "Next Order ID"
msgstr "Seuraava tilausnumero"

#: ./browser/viewlets/order-listing.pt:11
msgid "No."
msgstr "Nr."

#: ./profiles/default/types/collective.cart.core.Order.xml
msgid "Order"
msgstr "Tilaus"

#: ./profiles/default/types/collective.cart.core.OrderArticle.xml
msgid "Order Article"
msgstr "Tilaustuote"

#: ./profiles/default/types/collective.cart.core.OrderContainer.xml
msgid "Order Container"
msgstr "Tilauskonteineri"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Order Default Workflow"
msgstr "Tilauksen oletus-workflow"

#: ./browser/view.py:37
msgid "Order Listing"
msgstr "Tilauslista"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Ordered"
msgstr "Tilattu"

#: ./profiles/default/actions.xml
msgid "Orders"
msgstr "Tilaukset"

#: ./browser/viewlets/order-listing.pt:13
msgid "Owner"
msgstr "Omistaja"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Paid"
msgstr "Maksettu"

#: ./browser/viewlets/cart-article-listing.pt:15
msgid "Remove"
msgstr "Poista"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Returned"
msgstr "Palautettu"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "Shipped"
msgstr "Lähetetty"

#: ./profiles/default/actions.xml
msgid "Show list of orders."
msgstr "Näytä tilauksien lista."

#: ./browser/viewlets/order-listing.pt:14
msgid "State"
msgstr "Tila"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to Paid"
msgstr "Maksettu tilaan"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to Returned"
msgstr "Palautettu tilaan"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to Shipped"
msgstr "Lähetetty tilaan"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to canceled"
msgstr "Peruttu tilaan"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to charged"
msgstr "Laskutettu tilaan"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to created"
msgstr "Luotu tilaan"

#: ./profiles/default/workflows/order_default_workflow/definition.xml
msgid "State to ordered"
msgstr "Tilattu tilaan"

#: ./browser/viewlets/order-article-listing.pt:2
msgid "There are no articles within this cart."
msgstr "Tässä ostoskorissa ei ole yhtään tuotetta."

#: ./subscriber.py:22
msgid "This container is no longer a shopping site."
msgstr "Tämä konteineri ei enää ole kauppa."

#: ./profiles/default/actions.xml
msgid "Unmake Shopping Site"
msgstr "Lopeta kauppa"

#: ./profiles/default/actions.xml
msgid "Unmake this container shopping site."
msgstr "Otetaan tästä konteinerista kauppa ominaisuus pois."

#: ./browser/viewlets/order-listing.pt:12
msgid "Updated"
msgstr "Päivitetty"

#. Default: "The next order ID: ${order_id}"
#: ./browser/view.py:42
msgid "next_order_id_description"
msgstr "Seuraava tilaustunnus: ${order_id}"

#. Default: "There are no orders."
#: ./browser/viewlets/order-listing.pt:2
msgid "no-orders"
msgstr "Ei ole tilausta."

#. Default: "The order ID is already in use."
#: ./validator.py:18
msgid "order-id-already-in-use"
msgstr "Tilausnumero on jo käytössä."

#. Default: "Order ID: ${order_id}"
#: ./browser/view.py:63
msgid "order_view_title"
msgstr "Tilaustunnus: ${order_id}"

